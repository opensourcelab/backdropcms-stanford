# Backdrop CMS Stanford

This project includes https://backdropcms.org/ core, Stanford Decanter theme(https://github.com/SU-SWS/Awesome-Decanter/?tab=readme-ov-file#themestoolsframeworks), and two modules for upgrade from Drupal 7 to Backdrop CMS

- D2B module for WebUI content upgrade
- Code Upgrader module for upgrading custom code.

## Test and Submit Issues

This project includes development modules.  Please test and submit issues.

## Contributing

Pull requests are welcome. Please open an issue first to discuss what you would like to change.

## License

Reference Backdrop CMS and Decanter framework for details on licensing. 